from sre_constants import ASSERT
from flask import Flask, url_for, render_template, flash, request, redirect


import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin import storage

import docx
from pyrebase import pyrebase
from flask_caching import Cache
import requests
from docxtpl import DocxTemplate, InlineImage
from docx.shared import Mm
import io


# Use a service account
cred = credentials.Certificate('purplewallet-905a2-firebase-adminsdk-f2w1c-86dc6ac8fa.json')
firebase_admin.initialize_app(cred, {
      'storageBucket': 'purplewallet-905a2.appspot.com'})

db = firestore.client()

app = Flask(__name__)

firebaseConfig = {
    'apiKey': "AIzaSyBAeiYMa_x2aumGc3c7VjoLkxBXIqXy9fk",
    'authDomain': "purplewallet-905a2.firebaseapp.com",
    'databaseURL': "https://purplewallet-905a2-default-rtdb.firebaseio.com",
    'storageBucket': "purplewallet-905a2.appspot.com",
    'messagingSenderId': "765218494941",
    'appId': "1:765218494941:web:e365840be361d8d57fbe34",
}

firebase = pyrebase.initialize_app(firebaseConfig)

cache = Cache(config={'CACHE_TYPE': 'null'})

cache.init_app(app)

storage = firebase.storage()

#import PyPDF2
#from fpdf import FPDF

@app.route('/dashboard', methods=["GET", "POST"])
def dashboard():
    return render_template('index.html')

@app.route('/home', methods=["GET", "POST"])
def home():
    if request.method == "POST":
        #print("Hello")
        username = request.form["username"]
        password = request.form["password"]
        if username == 'consult' and password == 'Admin@1234':
            from firebase_admin import storage
            bucket = storage.bucket('purplewallet-905a2.appspot.com')
            blob= bucket.get_blob("AssetPhotos/8uDA44bHvDO21nVemtxDbcWZxlF3/335ac0c2-ff81-4d6f-b238-df0c7743dc7c/inside/inside_0")
            print("Metadata: {}".format(blob.metadata))


            db = firebase.database()

            all_users = len(db.child('users').get().val())
            #smooth_users = storage.child("executive_summary_smoothened").list_blobs()
            print("All Users", all_users)
            return render_template('index.html')
        else:
            return render_template('auth_login.html')



@app.route('/', methods=["GET", "POST"])
def login():
    return render_template('auth_login.html')

@app.route('/delete_asset/<user_data>/<asset_id>/<asset_name>', methods=["GET", "POST"])
def delete_asset(user_data, asset_id, asset_name):

    from firebase_admin import storage as admin_storage
    bucket = admin_storage.bucket()
    try:
        blob = bucket.blob("AssetPhotos/"+ user_data + "/" + asset_id + "/Map/Map" )
        #print(blob)
        blob.delete()
    except:
        pass
    try:
        blob = bucket.blob("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_0" )
        blob.delete()
    except:
        pass
    try:
        blob = bucket.blob("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_1" )
        blob.delete()
    except:
        pass
    try:
        blob = bucket.blob("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_2" )
        blob.delete()
    except:
        pass
    try:
        blob = bucket.blob("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_3" )
        blob.delete()
    except:
        pass
    try:
        blob = bucket.blob("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_4" )
        blob.delete()
    except:
        pass
    try:
        blob = bucket.blob("AssetPhotos/"+ user_data + "/" + asset_id + "/outside/outside_0")
        blob.delete()
    except:
        pass
    try:
        blob = bucket.blob("AssetPhotos/"+ user_data + "/" + asset_id + "/outside/outside_1")
        blob.delete()
    except:
        pass
    try:
        blob = bucket.blob("AssetPhotos/"+ user_data + "/" + asset_id + "/outside/outside_2")
        blob.delete()
    except:
        pass


    import re
    stripped_string = re.sub(r'\W+', '', asset_name)
    print(stripped_string)

    doc = db.document(u'users/' + str(user_data) + '/userPolicies/assets/assetDetails/'+str(asset_id))
    doc = doc.delete()

    doc = db.document(u'users/' + str(user_data) + '/userPolicies/assets')
    doc.update({db.field_path(asset_name) : firestore.DELETE_FIELD})

    doc = db.document(u'assets/'+ str(asset_id))
    doc = doc.delete()

    return render_template('index.html')

@app.route('/user_data_view/<user_data>/<asset_id>/<asset_name>', methods=["GET", "POST"])
def user_data_view(user_data, asset_id, asset_name):

    property_category_Q_and_A = [None] * 16
    facility_category_Q_and_A = [None] * 14
    fire_category_Q_and_A = [None] * 14
    flammable_category_Q_and_A = [None] * 8
    money_category_Q_and_A = [None] * 10
    perils_category_Q_and_A = [None] * 5
    security_category_Q_and_A = [None] * 21
    utilities_category_Q_and_A = [None] * 6
    ratings_category_Q_and_A = [None] * 15
    #executive_summary_category = [None] * 5
    general_info_category = [None] * 6
    u_data_Q_and_A = [None] * 4

    #Questions

    general_info = ["Asset Name", "Country", "Address", "Inspection Date", "surveyor", "contact_person"]

    #executive_summary_details = ["MPL", "MPL Desc", "EPL", "EPL Desc", "AGC"]

    ratings = ["Fire",
    "Smoke",
    "Explosion",
    "Lightning",
    "Flood",
    "Storm",
    "Goods",
    "Electric",
    "Falling Trees",
    "Malicious Damage",
    "Theft",
    "Inception Hazards",
    "Spread Hazards",
    "Fire Load",
    "Planned Structural/Occupational changes"]

    
    prop = ['Please mark the location of your asset',
            'Select the construction type of your asset',
            'Kindly give the building size (sq. m)',
            'Where is the nearest building located(in metre)',
            'Kindly classify the occupation carried in your asset',
            'Kindly specify the roof construction',
            'Kindly specify the age of the building (in years)',
            'Please specify the number of floors present (Including Ground Floor)',
            'Is it a multi-tenant site ?',
            'What is the surrounding area zoning?',
            'How is the site accessed?',
            'What is the number of staff per shift ?',
            'How many days is business operated per week?',
            'What is opening time of your business?',
            'What is closing time of your business?',
            'property remarks']

    facility = ['Are goods stored?',
                'Please select the kind of goods stored',
                'Please mention the commodity stored',
                'Are there lifts or elevators?',
                'Is there any kitchen or pantry?',
                'Are there geysers?',
                'Are there stairways?',
                'Are there gas cylinders?',
                'Is there Ventilation System?',
                'Is there air conditioning facility?',
                'Is there smoking control system?',
                'Is there waste control system?',
                'What is the closing procedure of your property?',
                'facility remarks']

    fire = ['Do you have a fire extinguisher?',
            'By whom is it serviced?',
            'what is the last date of service?',
            'What is the SAQCC number?',
            'What is the number of external hose reels?',
            'What is the number of internal hose reels?',
            'Do you have smoke detectors?',
            'Is it serviced in the last 12 months?',
            'Do you have fire escapes?',
            'Do you have access to firebrigade ?',
            'Do you have fire department nearby',
            'What is the estimated distance of Fire brigade?',
            'Do you have space around your building for a fire truck to pass?',
            'fire remarks']

    flammable = ['Do you have flammable liquids in your premises?',
                'What are the types of flammable liquids?',
                'Kindly give the quantity being used',
                'What is the storage type used?',
                'Do you have Hazardous liquids in your premises?',
                'What is the type of Hazardous material stored?',
                'What is the storage type used ?',
                'flammable remarks']

    money = ['Is cash being handled at your property?',
            'How much cash is involved?',
            'How often do you do banking transactions?',
            'Is transit vehicle used for transportation of cash ?',
            'Mention the banking company name',
            'Is ATM present in your premises?',
            'Select the security system used in your ATM',
            'Do you have safe/strong room?',
            'Select your SABS category',
            'money remarks']

    perils = ['Choose communication systems availaible at your premises ',
            'Do you have television set installed in your premises?',
            ' Have there been instances of rain/lightning damaging your communication systems ?',
            'Choose the power backup system installed in your premises.',
            'perils remarks']

    security = ['Is your property parameter controlled ?',
                'Are there internal securities controlled ?',
                'Does your property have watchmen ?',
                'Is intruder alarm system installed ?',
                'What is the name of the company handling the security of your property?',
                'What is the response time ?',
                'How is the alarm notified ?',
                'Is it SAISDA approved?',
                'Is there security protection in roof void?',
                'Does your property have CCTV camera ?',
                'What is the Camera type?',
                'What is the observation area ?',
                'What is the backup frequency ?',
                'Are all open windows burglar barred ?',
                'Are all closed windows burglar barred ?',
                'Do all external gates have security guards ?',
                'Is there 24 hours security ?',
                'Is there full building perimeter alarm ?',
                'Is there high wall or electrified fence ?',
                'Is there vacant land surrounding your property?',
                'security remarks']

    utilities = ['Do you have electrical compliance certificate ?',
                'Do you have surge Protection ?',
                'Do you have main DB installed ?',
                'Do you have wall plugs installed ?',
                'utility remarks']

    u_data = ['email', 'name', 'phone', 'userRole']


    
    doc = db.document(u'users/'+str(user_data)+'/userPolicies/assets/assetDetails/'+str(asset_id)+'/smoothened_data/data')
    doc = doc.get()
    if doc.exists:
        data = doc.to_dict()

        #User Name
        d = db.document(u'users/'+str(user_data)+'/userDetails/general')
        d = d.get()
        if d.exists:
            user_details = d.to_dict()
            for ques, ans in user_details.items():
                if ques in u_data:
                        index_ques =  u_data.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        u_data_Q_and_A[index_ques] = cat_temp


        #print(q_and_a)
        for key, value in data.items():
            if key == 'ratings':
                for ques, ans in value.items():
                    if ques in ratings:
                        index_ques =  ratings.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        ratings_category_Q_and_A[index_ques] = cat_temp

            if key == 'General Information':
                for ques, ans in value.items():
                    if ques in general_info:
                        index_ques =  general_info.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        general_info_category[index_ques] = cat_temp

            if key == 'property':
                for ques, ans in value.items():
                    if ques in prop:
                        index_ques =  prop.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        property_category_Q_and_A[index_ques] = cat_temp
            if key == 'facility':
                for ques, ans in value.items():
                    if ques in facility:
                        index_ques =  facility.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        facility_category_Q_and_A[index_ques] = cat_temp

            if key == 'fire':
                for ques, ans in value.items():
                    if ques in fire:
                        index_ques =  fire.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        fire_category_Q_and_A[index_ques] = cat_temp

            if key == 'flammable':
                for ques, ans in value.items():
                    if ques in flammable:
                        index_ques =  flammable.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        flammable_category_Q_and_A[index_ques] = cat_temp

            if key == 'money':
                for ques, ans in value.items():
                    if ques in money:
                        index_ques =  money.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        money_category_Q_and_A[index_ques] = cat_temp

            if key == 'perils':
                for ques, ans in value.items():
                    if ques in perils:
                        index_ques =  perils.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        perils_category_Q_and_A[index_ques] = cat_temp

            if key == 'security':
                for ques, ans in value.items():
                    if ques in security:
                        index_ques =  security.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        security_category_Q_and_A[index_ques] = cat_temp

            if key == 'utilities':
                for ques, ans in value.items():
                    if ques in utilities:
                        index_ques =  utilities.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        utilities_category_Q_and_A[index_ques] = cat_temp

        class_1 = "col-12 col-xl-6 col-lg-12 mb-xl-5 mb-5" 
        class_2 = "col-12 col-xl-6 col-lg-12 mb-xl-0 mb-0"

        img_url = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/Map/Map").get_url(None)

        img_url_1 = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_0" ).get_url(None)
        img_url_2 = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_1" ).get_url(None)
        img_url_3 = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_2" ).get_url(None)
        img_url_4 = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_3" ).get_url(None)
        img_url_5 = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_4" ).get_url(None)

        img_url_6 = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/outside/outside_0").get_url(None)
        img_url_7 = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/outside/outside_1").get_url(None)
        img_url_8 = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/outside/outside_2").get_url(None)

        return render_template('user_data_view.html',img_url = img_url,
        img_url_1 = img_url_1,
        img_url_2 = img_url_2,
        img_url_3 = img_url_3,
        img_url_4 = img_url_4,
        img_url_5 = img_url_5,
        img_url_6 = img_url_6,
        img_url_7 = img_url_7,
        img_url_8 = img_url_8,
        property_category_Q_and_A = property_category_Q_and_A,
        facility_category_Q_and_A = facility_category_Q_and_A,
        fire_category_Q_and_A = fire_category_Q_and_A,
        flammable_category_Q_and_A = flammable_category_Q_and_A,
        money_category_Q_and_A = money_category_Q_and_A,
        perils_category_Q_and_A = perils_category_Q_and_A,
        security_category_Q_and_A = security_category_Q_and_A,
        utilities_category_Q_and_A = utilities_category_Q_and_A,
        ratings_category_Q_and_A = ratings_category_Q_and_A,
        u_data_Q_and_A = u_data_Q_and_A, class_1 = class_1, class_2 = class_2)

          
    else:
        return render_template('error.html')
    #print(data)

@app.route('/upload_images/<user_data>/<asset_id>', methods=["GET", "POST"])
def upload_images(user_data, asset_id):
    #print(user_data)
    #print(asset_id)
    if request.method == "POST":

        inside_images = request.files.getlist('inside_images')
        outside_images = request.files.getlist('outside_images')
        map = request.files.getlist('map')
        

        for i in range(0, len(inside_images)):
            print(i)
            storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/" + "inside_" + str(i)).put(inside_images[i])

        for i in range(0, len(outside_images)):
            print(i)
            storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/outside/" + "outside_" + str(i)).put(outside_images[i])

        for i in range(0,len(map)):
            storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/Map/Map").put(map[i])

    return render_template('index.html')

@app.route('/download_data/<user_data>/<asset_id>/<asset_name>', methods=["GET", "POST"])
def download_data(user_data, asset_id, asset_name):
    
    data = ''
    property_category_Q_and_A = [None] * 16
    facility_category_Q_and_A = [None] * 14
    fire_category_Q_and_A = [None] * 14
    flammable_category_Q_and_A = [None] * 8
    money_category_Q_and_A = [None] * 10
    perils_category_Q_and_A = [None] * 5
    security_category_Q_and_A = [None] * 21
    utilities_category_Q_and_A = [None] * 5
    ratings__category_Q_and_A = [None] * 15
    #executive_summary_category = [None] * 5
    general_info_category = [None] * 6

    #Questions

    general_info = ["Asset Name", "Country", "Address", "Inspection Date", "surveyor", "contact_person"]

    #executive_summary_details = ["MPL", "MPL Desc", "EPL", "EPL Desc", "AGC"]

    ratings = ["Fire",
    "Smoke",
    "Explosion",
    "Lightning",
    "Flood",
    "Storm",
    "Goods",
    "Electric",
    "Falling Trees",
    "Malicious Damage",
    "Theft",
    "Inception Hazards",
    "Spread Hazards",
    "Fire Load",
    "Planned Structural/Occupational changes"]

    
    prop = ['Please mark the location of your asset',
            'Select the construction type of your asset',
            'Kindly give the building size (sq. m)',
            'Where is the nearest building located(in metre)',
            'Kindly classify the occupation carried in your asset',
            'Kindly specify the roof construction',
            'Kindly specify the age of the building (in years)',
            'Please specify the number of floors present (Including Ground Floor)',
            'Is it a multi-tenant site ?',
            'What is the surrounding area zoning?',
            'How is the site accessed?',
            'What is the number of staff per shift ?',
            'How many days is business operated per week?',
            'What is opening time of your business?',
            'What is closing time of your business?',
            'property remarks']

    facility = ['Are goods stored?',
                'Please select the kind of goods stored',
                'Please mention the commodity stored',
                'Are there lifts or elevators?',
                'Is there any kitchen or pantry?',
                'Are there geysers?',
                'Are there stairways?',
                'Are there gas cylinders?',
                'Is there Ventilation System?',
                'Is there air conditioning facility?',
                'Is there smoking control system?',
                'Is there waste control system?',
                'What is the closing procedure of your property?',
                'facility remarks']

    fire = ['Do you have a fire extinguisher?',
            'By whom is it serviced?',
            'what is the last date of service?',
            'What is the SAQCC number?',
            'What is the number of external hose reels?',
            'What is the number of internal hose reels?',
            'Do you have smoke detectors?',
            'Is it serviced in the last 12 months?',
            'Do you have fire escapes?',
            'Do you have access to firebrigade ?',
            'Do you have fire department nearby',
            'What is the estimated distance of Fire brigade?',
            'Do you have space around your building for a fire truck to pass?',
            'fire remarks']

    flammable = ['Do you have flammable liquids in your premises?',
                'What are the types of flammable liquids?',
                'Kindly give the quantity being used',
                'What is the storage type used?',
                'Do you have Hazardous liquids in your premises?',
                'What is the type of Hazardous material stored?',
                'What is the storage type used ?',
                'flammable remarks']

    money = ['Is cash being handled at your property?',
            'How much cash is involved?',
            'How often do you do banking transactions?',
            'Is transit vehicle used for transportation of cash ?',
            'Mention the banking company name',
            'Is ATM present in your premises?',
            'Select the security system used in your ATM',
            'Do you have safe/strong room?',
            'Select your SABS category',
            'money remarks']

    perils = ['Choose communication systems availaible at your premises ',
            'Do you have television set installed in your premises?',
            ' Have there been instances of rain/lightning damaging your communication systems ?',
            'Choose the power backup system installed in your premises.',
            'perils remarks']

    security = ['Is your property parameter controlled ?',
                'Are there internal securities controlled ?',
                'Does your property have watchmen ?',
                'Is intruder alarm system installed ?',
                'What is the name of the company handling the security of your property?',
                'What is the response time ?',
                'How is the alarm notified ?',
                'Is it SAISDA approved?',
                'Is there security protection in roof void?',
                'Does your property have CCTV camera ?',
                'What is the Camera type?',
                'What is the observation area ?',
                'What is the backup frequency ?',
                'Are all open windows burglar barred ?',
                'Are all closed windows burglar barred ?',
                'Do all external gates have security guards ?',
                'Is there 24 hours security ?',
                'Is there full building perimeter alarm ?',
                'Is there high wall or electrified fence ?',
                'Is there vacant land surrounding your property?',
                'security remarks']

    utilities = ['Do you have electrical compliance certificate ?',
                'Do you have surge Protection ?',
                'Do you have main DB installed ?',
                'Do you have wall plugs installed ?',
                'utility remarks']

    doc = db.document(u'users/'+str(user_data)+'/userPolicies/assets/assetDetails/'+str(asset_id)+'/smoothened_data/data')
    doc = doc.get()
    if doc.exists:

        data = doc.to_dict()

        for key, value in data.items():      
            if key == 'ratings':
                for ques, ans in value.items():
                    if ques in ratings:
                        index_ques =  ratings.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        ratings__category_Q_and_A[index_ques] = cat_temp
    
            if key == 'General Information':
                for ques, ans in value.items():
                    if ques in general_info:
                        index_ques =  general_info.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        general_info_category[index_ques] = cat_temp
    
            if key == 'property':
                for ques, ans in value.items():
                    if ques in prop:
                        index_ques =  prop.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        property_category_Q_and_A[index_ques] = cat_temp
            if key == 'facility':
                for ques, ans in value.items():
                    if ques in facility:
                        index_ques =  facility.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        facility_category_Q_and_A[index_ques] = cat_temp
    
            if key == 'fire':
                for ques, ans in value.items():
                    if ques in fire:
                        index_ques =  fire.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        fire_category_Q_and_A[index_ques] = cat_temp
    
            if key == 'flammable':
                for ques, ans in value.items():
                    if ques in flammable:
                        index_ques =  flammable.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        flammable_category_Q_and_A[index_ques] = cat_temp
    
            if key == 'money':
                for ques, ans in value.items():
                    if ques in money:
                        index_ques =  money.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        money_category_Q_and_A[index_ques] = cat_temp
    
            if key == 'perils':
                for ques, ans in value.items():
                    if ques in perils:
                        index_ques =  perils.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        perils_category_Q_and_A[index_ques] = cat_temp
    
            if key == 'security':
                for ques, ans in value.items():
                    if ques in security:
                        index_ques =  security.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        security_category_Q_and_A[index_ques] = cat_temp
    
            if key == 'utilities':
                for ques, ans in value.items():
                    if ques in utilities:
                        index_ques =  utilities.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        utilities_category_Q_and_A[index_ques] = cat_temp
        
        q = ''
        a = ''
        q_a = ''
        #storage.child('example/Output.docx').put(excelFile)
        url = storage.child('executive_summary_smoothened/template/es_new.docx').get_url(None)
        response = requests.get(url, stream=True)
        document = io.BytesIO(response.content)

        doc = DocxTemplate(document)
        
        #a = {item[0]: item[1] for item in general_info_category}
        all_info = {item[0]: item[1] for item in general_info_category}
        property_category_Q_and_A = {item[0]: item[1] for item in property_category_Q_and_A}
        facility_category_Q_and_A = {item[0]: item[1] for item in facility_category_Q_and_A}
        fire_category_Q_and_A = {item[0]: item[1] for item in fire_category_Q_and_A}
        flammable_category_Q_and_A = {item[0]: item[1] for item in flammable_category_Q_and_A}
        money_category_Q_and_A = {item[0]: item[1] for item in money_category_Q_and_A}
        perils_category_Q_and_A = {item[0]: item[1] for item in perils_category_Q_and_A}
        security_category_Q_and_A = {item[0]: item[1] for item in security_category_Q_and_A}
        utilities_category_Q_and_A = {item[0]: item[1] for item in utilities_category_Q_and_A}
        ratings__category_Q_and_A = {item[0]: item[1] for item in ratings__category_Q_and_A}

        all_info.update(property_category_Q_and_A) 
        all_info.update(facility_category_Q_and_A)
        all_info.update(fire_category_Q_and_A)
        all_info.update(flammable_category_Q_and_A)
        all_info.update(money_category_Q_and_A)
        all_info.update(perils_category_Q_and_A)
        all_info.update(security_category_Q_and_A)
        all_info.update(utilities_category_Q_and_A)
        all_info.update(ratings__category_Q_and_A)

        map_list = ['asset_name', 'country', 'address', 'i_date', 'surveyor', 'contact_person',
        'p_1',
        'p_2',
        'p_3',
        'p_4',
        'p_5',
        'p_6',
        'p_7',
        'p_8',
        'p_9',
        'p_10',
        'p_11',
        'p_12',
        'p_13',
        'p_14',
        'p_15',
        'p_16',
        'fa_1',
        'fa_2',
        'fa_3',
        'fa_4',
        'fa_5',
        'fa_6',
        'fa_7',
        'fa_8',
        'fa_9',
        'fa_10',
        'fa_11',
        'fa_12',
        'fa_13',
        'fa_14',
        'fi_1',
        'fi_2',
        'fi_3',
        'fi_4',
        'fi_5',
        'fi_6',
        'fi_7',
        'fi_8',
        'fi_9',
        'fi_10',
        'fi_11',
        'fi_12',
        'fi_13',
        'fi_14',
        'fl_1',
        'fl_2',
        'fl_3',
        'fl_4',
        'fl_5',
        'fl_6',
        'fl_7',
        'fl_8',
        'mo_1',
        'mo_2',
        'mo_3',
        'mo_4',
        'mo_5',
        'mo_6',
        'mo_7',
        'mo_8',
        'mo_9',
        'mo_10',
        'pe_1',
        'pe_2',
        'pe_3',
        'pe_4',
        'pe_5',
        'se_1',
        'se_2',
        'se_3',
        'se_4',
        'se_5',
        'se_6',
        'se_7',
        'se_8',
        'se_9',
        'se_10',
        'se_11',
        'se_12',
        'se_13',
        'se_14',
        'se_15',
        'se_16',
        'se_17',
        'se_18',
        'se_19',
        'se_20',
        'se_21',
        'ut_1',
        'ut_3',
        'ut_4',
        'ut_5',
        'ut_6',
        "Fire",
        "Smoke",
        "Explosion",
        "Lightning",
        "Flood",
        "Storm",
        "Goods",
        "Electric",
        "Falling_Trees",
        "Malicious_Damage",
        "Theft",
        "Inception_Hazards",
        "Spread_Hazards",
        "Fire_Load",
        "Planned_changes"]


        final_dict = dict(zip(map_list, list(all_info.values())))

        img_url = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/Map/Map").get_url(None)
        response = requests.get(img_url, stream=True)
        image = io.BytesIO(response.content)
        final_dict['map'] = InlineImage(doc, image, width=Mm(60), height=Mm(60))

        
        img_url = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_0" ).get_url(None)
        response = requests.get(img_url, stream=True)
        image_inside_1 = io.BytesIO(response.content)
        final_dict['inside_1'] = InlineImage(doc, image_inside_1, width=Mm(100), height=Mm(100))

        img_url = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_1" ).get_url(None)
        response = requests.get(img_url, stream=True)
        image_inside_2 = io.BytesIO(response.content)
        final_dict['inside_2'] = InlineImage(doc, image_inside_2, width=Mm(100), height=Mm(100))

        img_url = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_2" ).get_url(None)
        response = requests.get(img_url, stream=True)
        image_inside_3 = io.BytesIO(response.content)
        final_dict['inside_3'] = InlineImage(doc, image_inside_3, width=Mm(100), height=Mm(100))
        
        img_url = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_3" ).get_url(None)
        response = requests.get(img_url, stream=True)
        image_inside_4 = io.BytesIO(response.content)
        final_dict['inside_4'] = InlineImage(doc, image_inside_4, width=Mm(100), height=Mm(100))

        img_url = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/inside_4" ).get_url(None)
        response = requests.get(img_url, stream=True)
        image_inside_5 = io.BytesIO(response.content)
        final_dict['inside_5'] = InlineImage(doc, image_inside_5, width=Mm(100), height=Mm(100))
    
        img_url = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/outside/outside_0").get_url(None)
        response = requests.get(img_url, stream=True)
        image_outside_1 = io.BytesIO(response.content)
        final_dict['outside_1'] = InlineImage(doc, image_outside_1, width=Mm(100), height=Mm(100))

        img_url = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/outside/outside_1").get_url(None)
        response = requests.get(img_url, stream=True)
        image_outside_2 = io.BytesIO(response.content)
        final_dict['outside_2'] = InlineImage(doc, image_outside_2, width=Mm(100), height=Mm(100))

        img_url = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/outside/outside_2").get_url(None)
        response = requests.get(img_url, stream=True)
        image_outside_3 = io.BytesIO(response.content)
        final_dict['outside_3'] = InlineImage(doc, image_outside_3, width=Mm(100), height=Mm(100))
    
        #context = { 'warehouse_name' : "ETG", 'country' : "India", 'warehouse_address' : "India Warehouse", 'warehouse_address_1' : "India Warehouse bangalore"}

        #a = {'warehouse_address_1' : "India Warehouse bangalore"}
        #print(final_dict)
        doc.render(final_dict)

        # Create in-memory buffer
        file_stream = io.BytesIO()
        # Save the .docx to the buffer
        doc.save(file_stream)
        # Reset the buffer's file-pointer to the beginning of the file
        file_stream.seek(0)

        storage.child('executive_summary_smoothened/template/' + user_data + '/' + asset_id  + '/' + asset_name + '.docx').put(file_stream)
        download = storage.child('executive_summary_smoothened/template/'+ user_data + '/' + asset_id + '/' + asset_name + '.docx').get_url(None)

    #mydoc = docx.Document()
#
    #mydoc.add_heading("General Information", 2)
    #for i in general_info_category:
    #    q = ''.join(i[0])
    #    a = ''.join(i[1])
    #    q_a = q + ":" + a
    #    mydoc.add_paragraph(str(q_a))

    
     # Image with defined size
    #mydoc.add_heading('Asset Address', 2)
    #
    #img_url = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/Map/Map").get_url(None)
#
    #
#
    #response = requests.get(img_url, stream=True)
    #image = io.BytesIO(response.content)
    #
    #mydoc.add_picture(image, width=Inches(2), height=Inches(2))


    #mydoc.add_heading("Property Details", 2)
    #for i in property_category_Q_and_A:
    #    q = ''.join(i[0])
    #    a = ''.join(i[1])
    #    q_a = q + ":" + a
    #    mydoc.add_paragraph(q_a)
#
    #mydoc.add_heading("Facilities", 2)
    #for i in facility_category_Q_and_A:
    #    q = ''.join(i[0])
    #    a = ''.join(i[1])
    #    q_a = q + ":" + a
    #    mydoc.add_paragraph(q_a)
    #
    #mydoc.add_heading("Fire Protection", 2)
    #for i in fire_category_Q_and_A:
    #    q = ''.join(i[0])
    #    a = ''.join(i[1])
    #    q_a = q + ":" + a
    #    mydoc.add_paragraph(q_a)
#
    #flammable_category_Q_and_A[6] = ['What is the storage type used?', ' ']
    #mydoc.add_heading("Flammable& Hazardous materials", 2)
    #for i in flammable_category_Q_and_A:
    #    q_a = ''
    #    q = ''.join(i[0])
    #    a = ''.join(i[1])
    #    q_a = q + ":" + a
    #    mydoc.add_paragraph(q_a)
#
    #mydoc.add_heading("Money", 2)
    #for i in money_category_Q_and_A:
    #    q = ''.join(i[0])
    #    a = ''.join(i[1])
    #    q_a = q + ":" + a
    #    mydoc.add_paragraph(q_a)
#
    #mydoc.add_heading("Perils", 2)
    #for i in perils_category_Q_and_A:
    #    q = ''.join(i[0])
    #    a = ''.join(i[1])
    #    q_a = q + ":" + a
    #    mydoc.add_paragraph(q_a)
#
    #mydoc.add_heading("Security", 2)
    #for i in security_category_Q_and_A:
    #    q = ''.join(i[0])
    #    a = ''.join(i[1])
    #    q_a = q + ":" + a
    #    mydoc.add_paragraph(q_a)
#
    #mydoc.add_heading("Utilities", 2)
    #for i in utilities_category_Q_and_A:
    #    q = ''.join(i[0])
    #    a = ''.join(i[1])
    #    q_a = q + ":" + a
    #    mydoc.add_paragraph(q_a)
#
    #mydoc.add_heading("Risk Classification", 2)
    #for i in ratings_category_Q_and_A:
    #    q = ''.join(i[0])
    #    a = ''.join(i[1])
    #    q_a = q + ":" + a
    #    mydoc.add_paragraph(q_a)

    #mydoc.add_heading("General Risk Summary & Comments", 2)
    #for i in executive_summary_category:
    #    q = ''.join(i[0])
    #    a = ''.join(i[1])
    #    q_a = q + ":" + a
    #    mydoc.add_paragraph(q_a)

    #storage.child("Assetphotos/" + user_data + "/" + asset_id + "images/example.jpg")

    # Image with defined size
    #mydoc.add_heading('Property Details', 2)
    #mydoc.add_heading('Internal', 3)
    

    #for i in range(2):
    #    img_url = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/inside/" + "inside_" + str(i)).get_url(None)
    #    response = requests.get(img_url, stream=True)
    #    image_inside = io.BytesIO(response.content)
#
    #    mydoc.add_picture(image_inside, width=Inches(4), height=Inches(4))
#
    #mydoc.add_heading('External', 3)
    #for i in range(2):
    #    img_url = storage.child("AssetPhotos/"+ user_data + "/" + asset_id + "/outside/" + "outside_" + str(i)).get_url(None)
    #    response = requests.get(img_url, stream=True)
    #    image_outside = io.BytesIO(response.content)
#
    #    mydoc.add_picture(image_outside, width=Inches(4), height=Inches(4))


    #mydoc.save(asset_name + ".docx")

    else:
        print(u'No such document!')

    #return render_template('index.html')
    return redirect(download)


@app.route('/user_data_update/<user_data>/<asset_id>', methods=["GET", "POST"])
def user_data_update(user_data, asset_id):
    #Questions
    prop = ['Please mark the location of your asset',
            'Select the construction type of your asset',
            'Kindly give the building size (sq. m)',
            'Where is the nearest building located(in metre)',
            'Kindly classify the occupation carried in your asset',
            'Kindly specify the roof construction',
            'Kindly specify the age of the building (in years)',
            'Please specify the number of floors present (Including Ground Floor)',
            'Is it a multi-tenant site ?',
            'What is the surrounding area zoning?',
            'How is the site accessed?',
            'What is the number of staff per shift ?',
            'How many days is business operated per week?',
            'What is opening time of your business?',
            'What is closing time of your business?']

    facility = ['Are goods stored?',
                'Please select the kind of goods stored',
                'Please mention the commodity stored',
                'Are there lifts or elevators?',
                'Is there any kitchen or pantry?',
                'Are there geysers?',
                'Are there stairways?',
                'Are there gas cylinders?',
                'Is there Ventilation System?',
                'Is there air conditioning facility?',
                'Is there smoking control system?',
                'Is there waste control system?',
                'What is the closing procedure of your property?']

    fire = ['Do you have a fire extinguisher?',
            'By whom is it serviced?',
            'what is the last date of service?',
            'What is the SAQCC number?',
            'What is the number of external hose reels?',
            'What is the number of internal hose reels?',
            'Do you have smoke detectors?',
            'Is it serviced in the last 12 months?',
            'Do you have fire escapes?',
            'Do you have access to firebrigade ?',
            'Do you have fire department nearby',
            'What is the estimated distance of Fire brigade?',
            'Do you have space around your building for a fire truck to pass?']

    flammable = ['Do you have flammable liquids in your premises?',
                'What are the types of flammable liquids?',
                'Kindly give the quantity being used',
                'What is the storage type used?',
                'Do you have Hazardous liquids in your premises?',
                'What is the type of Hazardous material stored?',
                'What is the storage type used ?']

    money = ['Is cash being handled at your property?',
            'How much cash is involved?',
            'How often do you do banking transactions?',
            'Is transit vehicle used for transportation of cash ?',
            'Mention the banking company name',
            'Is ATM present in your premises?',
            'Select the security system used in your ATM',
            'Do you have safe/strong room?',
            'Select your SABS category']

    perils = ['Choose communication systems availaible at your premises ',
            'Do you have television set installed in your premises?',
            ' Have there been instances of rain/lightning damaging your communication systems ?',
            'Choose the power backup system installed in your premises.']

    security = ['Is your property parameter controlled ?',
                'Are there internal securities controlled ?',
                'Does your property have watchmen ?',
                'Is intruder alarm system installed ?',
                'What is the name of the company handling the security of your property?',
                'What is the response time ?',
                'How is the alarm notified ?',
                'Is it SAISDA approved?',
                'Is there security protection in roof void?',
                'Does your property have CCTV camera ?',
                'What is the Camera type?',
                'What is the observation area ?',
                'What is the backup frequency ?',
                'Are all open windows burglar barred ?',
                'Are all closed windows burglar barred ?',
                'Do all external gates have security guards ?',
                'Is there 24 hours security ?',
                'Is there full building perimeter alarm ?',
                'Is there high wall or electrified fence ?',
                'Is there vacant land surrounding your property?']

    utilities = ['Do you have electrical compliance certificate ?',
                'Do you have surge Protection ?',
                'Do you have main DB installed ?',
                'Do you have wall plugs installed ?']

    if request.method == "POST":

        asset_name = request.form.get('asset_name')
        country = request.form.get('country')
        address = request.form.get('address')
        inspection_date = request.form.get('inspection_date')
        surveyor = request.form.get('surveyor')
        contact_person = request.form.get('contact_person')
        
        property_question_1 = request.form.get('property_1')
        property_question_2 = request.form.get('property_2')
        property_question_3 = request.form.get('property_3')
        property_question_4 = request.form.get('property_4')
        property_question_5 = request.form.get('property_5')
        property_question_6 = request.form.get('property_6')
        property_question_7 = request.form.get('property_7')
        property_question_8 = request.form.get('property_8')
        property_question_9 = request.form.get('property_9')
        property_question_10 = request.form.get('property_10')
        property_question_11 = request.form.get('property_11')
        property_question_12 = request.form.get('property_12')
        property_question_13 = request.form.get('property_13')
        property_question_14 = request.form.get('property_14')
        property_question_15 = request.form.get('property_15')

        facility_question_1 = request.form.get('facility_1')
        facility_question_2 = request.form.get('facility_2')
        facility_question_3 = request.form.get('facility_3')
        facility_question_4 = request.form.get('facility_4')
        facility_question_5 = request.form.get('facility_5')
        facility_question_6 = request.form.get('facility_6')
        facility_question_7 = request.form.get('facility_7')
        facility_question_8 = request.form.get('facility_8')
        facility_question_9 = request.form.get('facility_9')
        facility_question_10 = request.form.get('facility_10')
        facility_question_11 = request.form.get('facility_11')
        facility_question_12 = request.form.get('facility_12')
        facility_question_13 = request.form.get('facility_13')

        fire_question_1 = request.form.get('fire_1')
        fire_question_2 = request.form.get('fire_2')
        fire_question_3 = request.form.get('fire_3')
        fire_question_4 = request.form.get('fire_4')
        fire_question_5 = request.form.get('fire_5')
        fire_question_6 = request.form.get('fire_6')
        fire_question_7 = request.form.get('fire_7')
        fire_question_8 = request.form.get('fire_8')
        fire_question_9 = request.form.get('fire_9')
        fire_question_10 = request.form.get('fire_10')
        fire_question_11 = request.form.get('fire_11')
        fire_question_12 = request.form.get('fire_12')
        fire_question_13 = request.form.get('fire_13')

        flammable_question_1 = request.form.get('flammable_1')
        flammable_question_2 = request.form.get('flammable_2')
        flammable_question_3 = request.form.get('flammable_3')
        flammable_question_4 = request.form.get('flammable_4')
        flammable_question_5 = request.form.get('flammable_5')
        flammable_question_6 = request.form.get('flammable_6')
        flammable_question_7 = request.form.get('flammable_7')
        

        money_question_1 = request.form.get('money_1')
        money_question_2 = request.form.get('money_2')
        money_question_3 = request.form.get('money_3')
        money_question_4 = request.form.get('money_4')
        money_question_5 = request.form.get('money_5')
        money_question_6 = request.form.get('money_6')
        money_question_7 = request.form.get('money_7')
        money_question_8 = request.form.get('money_8')
        money_question_9 = request.form.get('money_9')

        perils_question_1 = request.form.get('perils_1')
        perils_question_2 = request.form.get('perils_2')
        perils_question_3 = request.form.get('perils_3')
        perils_question_4 = request.form.get('perils_4')

        security_question_1 = request.form.get('security_1')
        security_question_2 = request.form.get('security_2')
        security_question_3 = request.form.get('security_3')
        security_question_4 = request.form.get('security_4')
        security_question_5 = request.form.get('security_5')
        security_question_6 = request.form.get('security_6')
        security_question_7 = request.form.get('security_7')
        security_question_8 = request.form.get('security_8')
        security_question_9 = request.form.get('security_9')
        security_question_10 = request.form.get('security_10')
        security_question_11 = request.form.get('security_11')
        security_question_12 = request.form.get('security_12')
        security_question_13 = request.form.get('security_13')
        security_question_14 = request.form.get('security_14')
        security_question_15 = request.form.get('security_15')
        security_question_16 = request.form.get('security_16')
        security_question_17 = request.form.get('security_17')
        security_question_18 = request.form.get('security_18')
        security_question_19 = request.form.get('security_19')
        security_question_20 = request.form.get('security_20')

        utilities_question_1 = request.form.get('utilities_1')
        utilities_question_2 = request.form.get('utilities_2')
        utilities_question_3 = request.form.get('utilities_3')
        utilities_question_4 = request.form.get('utilities_4')

        ratings_question_1 = request.form.get('ratings_1')
        ratings_question_2 = request.form.get('ratings_2')
        ratings_question_3 = request.form.get('ratings_3')
        ratings_question_4 = request.form.get('ratings_4')
        ratings_question_5 = request.form.get('ratings_5')
        ratings_question_6 = request.form.get('ratings_6')
        ratings_question_7 = request.form.get('ratings_7')
        ratings_question_8 = request.form.get('ratings_8')
        ratings_question_9 = request.form.get('ratings_9')
        ratings_question_10 = request.form.get('ratings_10')
        ratings_question_11 = request.form.get('ratings_11')
        ratings_question_12 = request.form.get('ratings_12')
        ratings_question_13 = request.form.get('ratings_13')
        ratings_question_14 = request.form.get('ratings_14')
        ratings_question_15 = request.form.get('ratings_15')

        #MPL = request.form.get('MPL')
        #MPL_desc = request.form.get('MPL_desc')
        #EPL = request.form.get('EPL')
        #EPL_desc = request.form.get('EPL_desc')
        #AGC = request.form.get('AGC')

        property_remarks = request.form.get('property_remarks')
        facility_remarks = request.form.get('facility_remarks')
        fire_remarks = request.form.get('fire_remarks')
        flammable_remarks = request.form.get('flammable_remarks')
        money_remarks = request.form.get('money_remarks')
        perils_remarks = request.form.get('perils_remarks')
        security_remarks = request.form.get('security_remarks')
        utility_remarks = request.form.get('utility_remarks')

        property_question_16 = request.form.get('property_16')
        facility_question_14 = request.form.get('facility_14')
        fire_question_14 = request.form.get('fire_14')
        flammable_question_8 = request.form.get('flammable_8')
        money_question_10 = request.form.get('money_10')
        perils_question_5 = request.form.get('perils_5')
        security_question_21 = request.form.get('security_21')
        utilities_question_5 = request.form.get('utilities_5')

        if property_remarks == None:
            print("None value created")
            data = { 
                    "property": {
                        prop[0] : property_question_1, 
                        prop[1] : property_question_2,
                        prop[2] : property_question_3,
                        prop[3] : property_question_4,
                        prop[4] : property_question_5,
                        prop[5] : property_question_6,
                        prop[6] : property_question_7,
                        prop[7] : property_question_8,
                        prop[8] : property_question_9,
                        prop[9] : property_question_10,
                        prop[10] : property_question_11,
                        prop[11] : property_question_12,
                        prop[12] : property_question_13,
                        prop[13] : property_question_14,
                        prop[14] : property_question_15,
                        "property remarks" : property_question_16
                        }, 
                        
                    "facility": {
                        facility[0] : facility_question_1, 
                        facility[1] : facility_question_2,
                        facility[2] : facility_question_3,
                        facility[3] : facility_question_4,
                        facility[4] : facility_question_5,
                        facility[5] : facility_question_6,
                        facility[6] : facility_question_7,
                        facility[7] : facility_question_8,
                        facility[8] : facility_question_9,
                        facility[9] : facility_question_10,
                        facility[10] : facility_question_11,
                        facility[11] : facility_question_12,
                        facility[12] : facility_question_13,
                        "facility remarks" : facility_question_14
                        },

                    "fire": {
                        fire[0] : fire_question_1, 
                        fire[1] : fire_question_2,
                        fire[2] : fire_question_3,
                        fire[3] : fire_question_4,
                        fire[4] : fire_question_5,
                        fire[5] : fire_question_6,
                        fire[6] : fire_question_7,
                        fire[7] : fire_question_8,
                        fire[8] : fire_question_9,
                        fire[9] : fire_question_10,
                        fire[10] : fire_question_11,
                        fire[11] : fire_question_12,
                        fire[12] : fire_question_13,
                        "fire remarks" : fire_question_14
                        },

                    "flammable": {
                        flammable[0] : flammable_question_1, 
                        flammable[1] : flammable_question_2,
                        flammable[2] : flammable_question_3,
                        flammable[3] : flammable_question_4,
                        flammable[4] : flammable_question_5,
                        flammable[5] : flammable_question_6,
                        flammable[6] : flammable_question_7,
                        "flammable remarks" : flammable_question_8
                        },

                    "money": {
                        money[0] : money_question_1, 
                        money[1] : money_question_2,
                        money[2] : money_question_3,
                        money[3] : money_question_4,
                        money[4] : money_question_5,
                        money[5] : money_question_6,
                        money[6] : money_question_7,
                        money[7] : money_question_8,
                        money[8] : money_question_9,
                        "money remarks" : money_question_10
                        },

                    "perils": {
                        perils[0] : perils_question_1, 
                        perils[1] : perils_question_2,
                        perils[2] : perils_question_3,
                        perils[3] : perils_question_4,
                        "perils remarks" : perils_question_5 
                        },

                    "security": {
                        security[0] : security_question_1, 
                        security[1] : security_question_2,
                        security[2] : security_question_3,
                        security[3] : security_question_4,
                        security[4] : security_question_5,
                        security[5] : security_question_6,
                        security[6] : security_question_7,
                        security[7] : security_question_8,
                        security[8] : security_question_9,
                        security[9] : security_question_10,
                        security[10] : security_question_11,
                        security[11] : security_question_12,
                        security[12] : security_question_13,
                        security[13] : security_question_14,
                        security[14] : security_question_15,
                        security[15] : security_question_16,
                        security[16] : security_question_17,
                        security[17] : security_question_18,
                        security[18] : security_question_19,
                        security[19] : security_question_20,
                        "security remarks" : security_question_21
                        },

                    "utilities": {
                        utilities[0] : utilities_question_1, 
                        utilities[1] : utilities_question_2,
                        utilities[2] : utilities_question_3,
                        utilities[3] : utilities_question_4,
                        "utility remarks" : utilities_question_5
                        },

                    "ratings": {
                        "Fire" : ratings_question_1, 
                        "Smoke" : ratings_question_2,
                        "Explosion" : ratings_question_3,
                        "Lightning" : ratings_question_4,
                        "Flood" : ratings_question_5,
                        "Storm" : ratings_question_6,
                        "Goods" : ratings_question_7,
                        "Electric" : ratings_question_8,
                        "Falling Trees" : ratings_question_9,
                        "Malicious Damage" : ratings_question_10,
                        "Theft" : ratings_question_11,
                        "Inception Hazards" : ratings_question_12,
                        "Spread Hazards" : ratings_question_13,
                        "Fire Load" : ratings_question_14,
                        "Planned Structural/Occupational changes" : ratings_question_15
                        },

                    #"Executive Summary Details" :{
                    #    "MPL" : MPL,
                    #    "MPL Desc" : MPL_desc,
                    #    "EPL" : EPL,
                    #    "EPL Desc" : EPL_desc,
                    #    "AGC" : AGC
                    #    },

                    "General Information" :{
                        "Asset Name" : asset_name,
                        "Country" : country,
                        "Address" : address,
                        "Inspection Date" : inspection_date,
                        "surveyor" : surveyor,
                        "contact_person" : contact_person,
                        },
                }
            doc = db.document(u'users/'+str(user_data)+'/userPolicies/assets/assetDetails/'+str(asset_id)+'/smoothened_data/data').set(data)


        
        else:
            data = { 
                        "property": {
                            prop[0] : property_question_1, 
                            prop[1] : property_question_2,
                            prop[2] : property_question_3,
                            prop[3] : property_question_4,
                            prop[4] : property_question_5,
                            prop[5] : property_question_6,
                            prop[6] : property_question_7,
                            prop[7] : property_question_8,
                            prop[8] : property_question_9,
                            prop[9] : property_question_10,
                            prop[10] : property_question_11,
                            prop[11] : property_question_12,
                            prop[12] : property_question_13,
                            prop[13] : property_question_14,
                            prop[14] : property_question_15,
                            "property remarks" : property_remarks
                            }, 

                        "facility": {
                            facility[0] : facility_question_1, 
                            facility[1] : facility_question_2,
                            facility[2] : facility_question_3,
                            facility[3] : facility_question_4,
                            facility[4] : facility_question_5,
                            facility[5] : facility_question_6,
                            facility[6] : facility_question_7,
                            facility[7] : facility_question_8,
                            facility[8] : facility_question_9,
                            facility[9] : facility_question_10,
                            facility[10] : facility_question_11,
                            facility[11] : facility_question_12,
                            facility[12] : facility_question_13,
                            "facility remarks" : facility_remarks
                            },

                        "fire": {
                            fire[0] : fire_question_1, 
                            fire[1] : fire_question_2,
                            fire[2] : fire_question_3,
                            fire[3] : fire_question_4,
                            fire[4] : fire_question_5,
                            fire[5] : fire_question_6,
                            fire[6] : fire_question_7,
                            fire[7] : fire_question_8,
                            fire[8] : fire_question_9,
                            fire[9] : fire_question_10,
                            fire[10] : fire_question_11,
                            fire[11] : fire_question_12,
                            fire[12] : fire_question_13,
                            "fire remarks" : fire_remarks
                            },

                        "flammable": {
                            flammable[0] : flammable_question_1, 
                            flammable[1] : flammable_question_2,
                            flammable[2] : flammable_question_3,
                            flammable[3] : flammable_question_4,
                            flammable[4] : flammable_question_5,
                            flammable[5] : flammable_question_6,
                            flammable[6] : flammable_question_7,
                            "flammable remarks" : flammable_remarks
                            },

                        "money": {
                            money[0] : money_question_1, 
                            money[1] : money_question_2,
                            money[2] : money_question_3,
                            money[3] : money_question_4,
                            money[4] : money_question_5,
                            money[5] : money_question_6,
                            money[6] : money_question_7,
                            money[7] : money_question_8,
                            money[8] : money_question_9,
                            "money remarks" : money_remarks
                            },

                        "perils": {
                            perils[0] : perils_question_1, 
                            perils[1] : perils_question_2,
                            perils[2] : perils_question_3,
                            perils[3] : perils_question_4,
                            "perils remarks" : perils_remarks
                            },

                        "security": {
                            security[0] : security_question_1, 
                            security[1] : security_question_2,
                            security[2] : security_question_3,
                            security[3] : security_question_4,
                            security[4] : security_question_5,
                            security[5] : security_question_6,
                            security[6] : security_question_7,
                            security[7] : security_question_8,
                            security[8] : security_question_9,
                            security[9] : security_question_10,
                            security[10] : security_question_11,
                            security[11] : security_question_12,
                            security[12] : security_question_13,
                            security[13] : security_question_14,
                            security[14] : security_question_15,
                            security[15] : security_question_16,
                            security[16] : security_question_17,
                            security[17] : security_question_18,
                            security[18] : security_question_19,
                            security[19] : security_question_20,
                            "security remarks" : security_remarks
                            },

                        "utilities": {
                            utilities[0] : utilities_question_1, 
                            utilities[1] : utilities_question_2,
                            utilities[2] : utilities_question_3,
                            utilities[3] : utilities_question_4,
                            "utility remarks" : utility_remarks
                            },

                        "ratings": {
                            "Fire" : ratings_question_1, 
                            "Smoke" : ratings_question_2,
                            "Explosion" : ratings_question_3,
                            "Lightning" : ratings_question_4,
                            "Flood" : ratings_question_5,
                            "Storm" : ratings_question_6,
                            "Goods" : ratings_question_7,
                            "Electric" : ratings_question_8,
                            "Falling Trees" : ratings_question_9,
                            "Malicious Damage" : ratings_question_10,
                            "Theft" : ratings_question_11,
                            "Inception Hazards" : ratings_question_12,
                            "Spread Hazards" : ratings_question_13,
                            "Fire Load" : ratings_question_14,
                            "Planned Structural/Occupational changes" : ratings_question_15
                            },

                        #"Executive Summary Details" :{
                        #    "MPL" : MPL,
                        #    "MPL Desc" : MPL_desc,
                        #    "EPL" : EPL,
                        #    "EPL Desc" : EPL_desc,
                        #    "AGC" : AGC
                        #    },

                        "General Information" :{
                            "Asset Name" : asset_name,
                            "Country" : country,
                            "Address" : address,
                            "Inspection Date" : inspection_date,
                            "surveyor" : surveyor,
                            "contact_person" : contact_person,
                            },
                    }
            doc = db.document(u'users/'+str(user_data)+'/userPolicies/assets/assetDetails/'+str(asset_id)+'/smoothened_data/data').set(data)

    return render_template('form_fileupload.html', user_data=user_data, asset_id=asset_id)

@app.route('/user_data_edit/<user_data>/<asset_id>/<asset_name>', methods=["GET", "POST"])
def user_data_edit(user_data, asset_id, asset_name):
    #print(user_data)
    #print("====")
    #print(asset_id)
    #print("====")
    #print(asset_name)
    #Making the Q and A list with asset details
    q_and_a = []
    asset_temp = []

    

    doc_smooth = db.document(u'users/'+str(user_data)+'/userPolicies/assets/assetDetails/'+str(asset_id)+'/smoothened_data/data')
    doc_smooth = doc_smooth.get()

    if doc_smooth.exists:
        property_category_Q_and_A = [None] * 16
        facility_category_Q_and_A = [None] * 14
        fire_category_Q_and_A = [None] * 14
        flammable_category_Q_and_A = [None] * 8
        money_category_Q_and_A = [None] * 10
        perils_category_Q_and_A = [None] * 5
        security_category_Q_and_A = [None] * 21
        utilities_category_Q_and_A = [None] * 5
        ratings_category_Q_and_A = [None] * 15
        #executive_summary_category = [None] * 5
        general_info_category = [None] * 6
        u_data_Q_and_A = [None] * 4

        #Questions

        general_info = ["Asset Name", "Country", "Address", "Inspection Date", "surveyor", "contact_person"]

        #executive_summary_details = ["MPL", "MPL Desc", "EPL", "EPL Desc", "AGC"]

        ratings = ["Fire",
        "Smoke",
        "Explosion",
        "Lightning",
        "Flood",
        "Storm",
        "Goods",
        "Electric",
        "Falling Trees",
        "Malicious Damage",
        "Theft",
        "Inception Hazards",
        "Spread Hazards",
        "Fire Load",
        "Planned Structural/Occupational changes"]


        prop = ['Please mark the location of your asset',
                'Select the construction type of your asset',
                'Kindly give the building size (sq. m)',
                'Where is the nearest building located(in metre)',
                'Kindly classify the occupation carried in your asset',
                'Kindly specify the roof construction',
                'Kindly specify the age of the building (in years)',
                'Please specify the number of floors present (Including Ground Floor)',
                'Is it a multi-tenant site ?',
                'What is the surrounding area zoning?',
                'How is the site accessed?',
                'What is the number of staff per shift ?',
                'How many days is business operated per week?',
                'What is opening time of your business?',
                'What is closing time of your business?',
                'property remarks']

        facility = ['Are goods stored?',
                    'Please select the kind of goods stored',
                    'Please mention the commodity stored',
                    'Are there lifts or elevators?',
                    'Is there any kitchen or pantry?',
                    'Are there geysers?',
                    'Are there stairways?',
                    'Are there gas cylinders?',
                    'Is there Ventilation System?',
                    'Is there air conditioning facility?',
                    'Is there smoking control system?',
                    'Is there waste control system?',
                    'What is the closing procedure of your property?',
                    'facility remarks']

        fire = ['Do you have a fire extinguisher?',
                'By whom is it serviced?',
                'what is the last date of service?',
                'What is the SAQCC number?',
                'What is the number of external hose reels?',
                'What is the number of internal hose reels?',
                'Do you have smoke detectors?',
                'Is it serviced in the last 12 months?',
                'Do you have fire escapes?',
                'Do you have access to firebrigade ?',
                'Do you have fire department nearby',
                'What is the estimated distance of Fire brigade?',
                'Do you have space around your building for a fire truck to pass?',
                'fire remarks']

        flammable = ['Do you have flammable liquids in your premises?',
                    'What are the types of flammable liquids?',
                    'Kindly give the quantity being used',
                    'What is the storage type used?',
                    'Do you have Hazardous liquids in your premises?',
                    'What is the type of Hazardous material stored?',
                    'What is the storage type used ?',
                    'flammable remarks']

        money = ['Is cash being handled at your property?',
                'How much cash is involved?',
                'How often do you do banking transactions?',
                'Is transit vehicle used for transportation of cash ?',
                'Mention the banking company name',
                'Is ATM present in your premises?',
                'Select the security system used in your ATM',
                'Do you have safe/strong room?',
                'Select your SABS category',
                'money remarks']

        perils = ['Choose communication systems availaible at your premises ',
                'Do you have television set installed in your premises?',
                ' Have there been instances of rain/lightning damaging your communication systems ?',
                'Choose the power backup system installed in your premises.',
                'perils remarks']

        security = ['Is your property parameter controlled ?',
                    'Are there internal securities controlled ?',
                    'Does your property have watchmen ?',
                    'Is intruder alarm system installed ?',
                    'What is the name of the company handling the security of your property?',
                    'What is the response time ?',
                    'How is the alarm notified ?',
                    'Is it SAISDA approved?',
                    'Is there security protection in roof void?',
                    'Does your property have CCTV camera ?',
                    'What is the Camera type?',
                    'What is the observation area ?',
                    'What is the backup frequency ?',
                    'Are all open windows burglar barred ?',
                    'Are all closed windows burglar barred ?',
                    'Do all external gates have security guards ?',
                    'Is there 24 hours security ?',
                    'Is there full building perimeter alarm ?',
                    'Is there high wall or electrified fence ?',
                    'Is there vacant land surrounding your property?',
                    'security remarks']

        utilities = ['Do you have electrical compliance certificate ?',
                    'Do you have surge Protection ?',
                    'Do you have main DB installed ?',
                    'Do you have wall plugs installed ?',
                    'utility remarks']

        

        data = doc_smooth.to_dict()

 


        #print(q_and_a)
        for key, value in data.items():

            if key == 'ratings':
                for ques, ans in value.items():
                    if ques in ratings:
                        index_ques =  ratings.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        ratings_category_Q_and_A[index_ques] = cat_temp

            if key == 'General Information':
                for ques, ans in value.items():
                    if ques in general_info:
                        index_ques =  general_info.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        general_info_category[index_ques] = cat_temp

            if key == 'property':
                for ques, ans in value.items():
                    if ques in prop:
                        index_ques =  prop.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        property_category_Q_and_A[index_ques] = cat_temp
            if key == 'facility':
                for ques, ans in value.items():
                    if ques in facility:
                        index_ques =  facility.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        facility_category_Q_and_A[index_ques] = cat_temp

            if key == 'fire':
                for ques, ans in value.items():
                    if ques in fire:
                        index_ques =  fire.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        fire_category_Q_and_A[index_ques] = cat_temp

            if key == 'flammable':
                for ques, ans in value.items():
                    if ques in flammable:
                        index_ques =  flammable.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        flammable_category_Q_and_A[index_ques] = cat_temp

            if key == 'money':
                for ques, ans in value.items():
                    if ques in money:
                        index_ques =  money.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        money_category_Q_and_A[index_ques] = cat_temp

            if key == 'perils':
                for ques, ans in value.items():
                    if ques in perils:
                        index_ques =  perils.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        perils_category_Q_and_A[index_ques] = cat_temp

            if key == 'security':
                for ques, ans in value.items():
                    if ques in security:
                        index_ques =  security.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        security_category_Q_and_A[index_ques] = cat_temp

            if key == 'utilities':
                for ques, ans in value.items():
                    if ques in utilities:
                        index_ques =  utilities.index(ques)
                        cat_temp = []
                        cat_temp.append(ques)
                        cat_temp.append(ans)
                        utilities_category_Q_and_A[index_ques] = cat_temp

        counter = True
        asset_name_output = general_info_category[0]
        asset_name_output = asset_name_output[1]
        country_output = general_info_category[1]
        country_output = country_output[1]
        address_output = general_info_category[2]
        address_output = address_output[1]
        i_date = general_info_category[3]
        i_date = i_date[1]
        surveyor = general_info_category[4]
        surveyor = surveyor[1]
        contact = general_info_category[5]
        contact = contact[1]

        return render_template('user_data_edit.html',
        asset_name_output = asset_name_output, country_output = country_output, address_output = address_output,
        i_date = i_date, surveyor = surveyor, contact = contact,
        property_category_Q_and_A = property_category_Q_and_A,
        facility_category_Q_and_A = facility_category_Q_and_A,
        fire_category_Q_and_A = fire_category_Q_and_A,
        flammable_category_Q_and_A = flammable_category_Q_and_A,
        money_category_Q_and_A = money_category_Q_and_A,
        perils_category_Q_and_A = perils_category_Q_and_A,
        security_category_Q_and_A = security_category_Q_and_A,
        utilities_category_Q_and_A = utilities_category_Q_and_A,
        ratings_category_Q_and_A = ratings_category_Q_and_A,
        general_info_category = general_info_category, 
        user_data = user_data, asset_id = asset_id, asset_name=asset_name,
        counter = counter)


    else:
        counter = False

        doc = db.document(u'users/'+str(user_data)+'/userPolicies/assets/assetDetails/'+str(asset_id))
        doc = doc.get()
        asset_temp = asset_name
        asset_temp = { asset_temp : asset_id }
        q_and_a.append(asset_temp)
        q_and_a.append([doc.to_dict()])
        #print(q_and_a)
    

        #print(q_and_a)

        question_list = []
        answer_list = []
        final_list_main = []

        for count, main_list in enumerate(q_and_a):
            final_list = []
            #print(count)
            #print(main_list)
            if count % 2 == 0:
                final_list .append(main_list)
            else:
                #print(main_list)
                for sub_list in main_list:
                    #print(sub_list)
                    for cat_name, cat_value in sub_list.items():
                        #print(cat_value)
                        for sub_cat_key, sub_cat_value in cat_value.items():
                            temp_list = []
                            question_list = []
                            answer_list = []
                            text_list = []
                            #print(sub_cat_value)
                            temp_list.append(cat_name) 
                            for main_sub_list in sub_cat_value:
                                #print(main_sub_list)
                                for sub_sub_key, sub_sub_value in main_sub_list.items():
                                    #print(sub_sub_value)
                                    if sub_sub_key == 'question':
                                        question_list.append(sub_sub_value)
                                    elif sub_sub_key == 'text':
                                        text_list.append(sub_sub_value)
                                    elif sub_sub_key == 'answer':
                                        answer_list.append(sub_sub_value)
                            temp_list.append(question_list)
                            temp_list.append(text_list)
                            temp_list.append(answer_list)
                            final_list.append(temp_list)
            final_list_main.append(final_list)

        #print(final_list_main)

        #Questions
        prop = ['Please mark the location of your asset',
                'Select the construction type of your asset',
                'Kindly give the building size (sq. m)',
                'Where is the nearest building located(in metre)',
                'Kindly classify the occupation carried in your asset',
                'Kindly specify the roof construction',
                'Kindly specify the age of the building (in years)',
                'Please specify the number of floors present (Including Ground Floor)',
                'Is it a multi-tenant site ?',
                'What is the surrounding area zoning?',
                'How is the site accessed?',
                'What is the number of staff per shift ?',
                'How many days is business operated per week?',
                'What is opening time of your business?',
                'What is closing time of your business?']

        facility = ['Are goods stored?',
                    'Please select the kind of goods stored',
                    'Please mention the commodity stored',
                    'Are there lifts or elevators?',
                    'Is there any kitchen or pantry?',
                    'Are there geysers?',
                    'Are there stairways?',
                    'Are there gas cylinders?',
                    'Is there Ventilation System?',
                    'Is there air conditioning facility?',
                    'Is there smoking control system?',
                    'Is there waste control system?',
                    'What is the closing procedure of your property?']

        fire = ['Do you have a fire extinguisher?',
                'By whom is it serviced?',
                'what is the last date of service?',
                'What is the SAQCC number?',
                'What is the number of external hose reels?',
                'What is the number of internal hose reels?',
                'Do you have smoke detectors?',
                'Is it serviced in the last 12 months?',
                'Do you have fire escapes?',
                'Do you have access to firebrigade ?',
                'Do you have fire department nearby',
                'What is the estimated distance of Fire brigade?',
                'Do you have space around your building for a fire truck to pass?']

        flammable = ['Do you have flammable liquids in your premises?',
                    'What are the types of flammable liquids?',
                    'Kindly give the quantity being used',
                    'What is the storage type used?',
                    'Do you have Hazardous liquids in your premises?',
                    'What is the type of Hazardous material stored?',
                    'What is the storage type used ?']

        money = ['Is cash being handled at your property?',
                'How much cash is involved?',
                'How often do you do banking transactions?',
                'Is transit vehicle used for transportation of cash ?',
                'Mention the banking company name',
                'Is ATM present in your premises?',
                'Select the security system used in your ATM',
                'Do you have safe/strong room?',
                'Select your SABS category']

        perils = ['Choose communication systems availaible at your premises ',
                'Do you have television set installed in your premises?',
                ' Have there been instances of rain/lightning damaging your communication systems ?',
                'Choose the power backup system installed in your premises.']

        security = ['Is your property parameter controlled ?',
                    'Are there internal securities controlled ?',
                    'Does your property have watchmen ?',
                    'Is intruder alarm system installed ?',
                    'What is the name of the company handling the security of your property?',
                    'What is the response time ?',
                    'How is the alarm notified ?',
                    'Is it SAISDA approved?',
                    'Is there security protection in roof void?',
                    'Does your property have CCTV camera ?',
                    'What is the Camera type?',
                    'What is the observation area ?',
                    'What is the backup frequency ?',
                    'Are all open windows burglar barred ?',
                    'Are all closed windows burglar barred ?',
                    'Do all external gates have security guards ?',
                    'Is there 24 hours security ?',
                    'Is there full building perimeter alarm ?',
                    'Is there high wall or electrified fence ?',
                    'Is there vacant land surrounding your property?']

        utilities = ['Do you have electrical compliance certificate ?',
                    'Do you have surge Protection ?',
                    'Do you have main DB installed ?',
                    'Do you have wall plugs installed ?']

        prop_old = ['Please mark location of your asset',
                    'Select the construction type of your asset',
                    'Kindly give the building size (sq. m)',
                    'Where is the nearest building located(in metre)',
                    'Kindly classify the occupation carried in your asset',
                    'Kindly specify the roof construction',
                    'Kindly specify the age of the building (in years)',
                    'Please specify the number of floors present (Including Ground Floor)',
                    'Is multi-tenant site',
                    'What is the surrounding area zoning?',
                    'How is the site accessed?',
                    'What is the number of staff per shift ?',
                    'How many days is business operated per week?',
                    'What is the business opening time',
                    'What is the business closing time']

        facility_old = ['Are goods stored?',
                        'Please specify the Kind of goods stored',
                        '_',
                        'Are there lifts or elevators?',
                        'Is there any kitchen or pantry?',
                        'Are there geysers?',
                        'Are there stairways?',
                        'Are there gas cylinders?',
                        'Is there Ventilation System?',
                        'Is there air conditioning facility?',
                        'Is there smoking control system?',
                        'Is there waste control system?',
                        'whats the closing procedure of your property?']

        fire_old = ['Do you have a fire extinguisher?',
                    'By whom it is serviced by?',
                    'what is the last date of service?',
                    'What is the SAQCC number?',
                    'What is the number of external horse reels',
                    'What is the number of internal horse reels',
                    'Do you have smoke detectors',
                    'Is it serviced in last 12 months',
                    'Do you have fire escapes',
                    'Do you have access to firebrigade',
                    'Do you have fire department nearby',
                    'Whats the estimated distance of Fire brigade?',
                    'Do you have space around your building for a truck for pass']

        flammable_old=['Do you have flammable liquids in your premises?',
                        'What are the types of flammable liquids?',
                        'Kindly give the quantity being used',
                        'What is the storage type used?',
                        'Do you have Hazardous liquids in your premises?',
                        'What is the type of Hazardous material stored?',
                        'What is the storage type used?']

        money_old=['Is cash being handled',
                    'How much cash is involved?',
                    'How often do you do banking transactions?',
                    'Is transit vehicle used for transportation of cash ',
                    'Mention the banking company name',
                    'Is ATM present in your premises here?',
                    'Select the security system for your ATM ',
                    'Do you have safe/strong room',
                    'Select your SABS category']

        perils_old=['Choose communication systems availaible at your premises ',
                    'Do you have television set installed in your premises?',
                    ' Have there been instances of rain/lightning damaging your communication systems ?',
                    'Choose the power backup system installed in your premises.']

        security_old=['Is your property parameter controlled ?',
                        'Are there internal securities controlled ?',
                        'Does your property have watchmen ?',
                        'Is intruder alarm system installed ?',
                        'What is the name of the company handling security of your property ?',
                        'What is the response time ?',
                        'How is the alarm notified ?',
                        'Is it SAISDA approved?',
                        'Is there security protection in roof void?',
                        'Does your property have CCTV camera ?',
                        'What is the Camera type?',
                        'What is the observation area ?',
                        'What is the backup frequency ?',
                        'Are all open windows burglar barred ?',
                        'Are all closed windows burglar barred ?',
                        'Do all external gateds have security guards ?',
                        'Is there 24 hours security ?',
                        'Is there full building perimeter alarm ?',
                        'Is there high wall or electrified fence ?',
                        'Is there surrounding vacant land ?']

        utilities_old=['Do you have electrical compliance certificate ?',
                        'Do you have surge Protection ?',
                        'Do you have main DB installed ?',
                        'Do you have wall plugs installed ?',
                        ]

        property_category_Q_and_A = [None] * 15
        property_category_Q = []

        facility_category_Q_and_A = [None] * 13
        facility_category_Q = []

        fire_category_Q_and_A = [None] * 13
        fire_category_Q = []

        flammable_category_Q_and_A = [None] * 7
        flammable_category_Q = []

        money_category_Q_and_A = [None] * 9
        money_category_Q = []

        perils_category_Q_and_A = [None] * 4
        perils_category_Q = []

        security_category_Q_and_A = [None] * 20
        security_category_Q = []

        utilities_category_Q_and_A = [None] * 4
        utilities_category_Q = []

        #if only asset is added
        if final_list_main == []:
            print("The list is Empty")
            property_category_Q = prop
            facility_category_Q = facility
            fire_category_Q = fire
            flammable_category_Q = flammable
            money_category_Q = money
            perils_category_Q = perils
            security_category_Q = security
            utilities_category_Q = utilities

        for count, main_list in enumerate(final_list_main):
            if count % 2 == 0:
                for asset_list in main_list:
                    for name, id in asset_list.items():
                        print(name, ":" ,id)

            else:

                for i in main_list:

                    #PRPOERTY
                    if i[0] == 'property':
                        ques = ''.join(i[1])
                        text = ''.join(i[2])
                        #print(ques)
                        #print("===")
                        #print(text)
                        if len(ques) == 0 and len(text) != 0:
                            if text in prop:
                                index_ques = prop.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                property_category_Q_and_A[index_ques] = cat_temp

                            elif text in prop_old:
                                index_ques =  prop_old.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                property_category_Q_and_A[index_ques] = cat_temp

                            else:
                                if text == '':
                                    continue
                                elif text in prop:
                                    index_ques = prop.index(text)
                                    property_category_Q_and_A[index_ques] = cat_temp
                                elif text in prop_old:
                                    index_ques = prop_old.index(text)
                                    property_category_Q_and_A[index_ques] = cat_temp

                        elif ques != "@":
                            #print("yes", ques)
                            if ques in prop:
                                index_ques = prop.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                property_category_Q_and_A[index_ques] = cat_temp

                            elif ques in prop_old:
                                index_ques = prop_old.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                property_category_Q_and_A[index_ques] = cat_temp

                            else:
                                if ques == '':
                                    continue
                                elif ques in prop:
                                    index_ques = prop.index(ques)
                                    property_category_Q_and_A[index_ques] = cat_temp
                                elif ques in prop_old:
                                    index_ques = prop_old.index(ques)
                                    property_category_Q_and_A[index_ques] = cat_temp

                    #facility
                    if i[0] == 'facilities':
                        ques = ''.join(i[1])
                        text = ''.join(i[2])
                        if len(ques) == 0 and len(text) != 3:
                            if text in facility:
                                index_ques = facility.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                facility_category_Q_and_A[index_ques] = cat_temp

                            elif text in facility_old:
                                index_ques = facility_old.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                facility_category_Q_and_A[index_ques] = cat_temp

                            else:
                                if text == '':
                                    continue
                                elif text in facility:
                                    index_ques = facility.index(text)
                                    facility_category_Q_and_A[index_ques] = cat_temp
                                elif text in facility_old:
                                    index_ques = facility_old.index(text)
                                    facility_category_Q_and_A[index_ques] = cat_temp


                        elif ques != "@":
                            if ques in facility:
                                index_ques = facility.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                facility_category_Q_and_A[index_ques] = cat_temp
                            elif ques in facility_old:
                                index_ques = facility_old.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                facility_category_Q_and_A[index_ques] = cat_temp
                            else:
                                if ques == '':
                                    continue
                                elif ques in facility:
                                    index_ques = facility.index(ques)
                                    facility_category_Q_and_A[index_ques] = cat_temp
                                elif ques in facility_old:
                                    index_ques = facility_old.index(ques)
                                    facility_category_Q_and_A[index_ques] = cat_temp


                    #Fire
                    if i[0] == 'fire':
                        ques = ''.join(i[1])
                        text = ''.join(i[2])
                        if len(ques) == 0 and len(text) != 3:
                            if text in fire:
                                index_ques = fire.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                fire_category_Q_and_A[index_ques] = cat_temp
                            elif text in fire_old:
                                index_ques = fire_old.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                fire_category_Q_and_A[index_ques] = cat_temp
                            else:
                                if text == '':
                                    continue
                                elif text in fire:
                                    index_ques = fire.index(text)
                                    fire_category_Q_and_A[index_ques] = cat_temp
                                elif text in fire_old:
                                    index_ques = fire_old.index(text)
                                    fire_category_Q_and_A[index_ques] = cat_temp

                        elif ques != "@":
                            if ques in fire:
                                index_ques = fire.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                fire_category_Q_and_A[index_ques] = cat_temp
                            elif ques in fire_old:
                                index_ques = fire_old.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                fire_category_Q_and_A[index_ques] = cat_temp
                            else:
                                if ques == '':
                                    continue
                                elif ques in fire:
                                    index_ques = fire.index(ques)
                                    fire_category_Q_and_A[index_ques] = cat_temp
                                elif ques in fire_old:
                                    index_ques = fire_old.index(ques)
                                    fire_category_Q_and_A[index_ques] = cat_temp

                    #flammable
                    if i[0] == 'flammable':
                        ques = ''.join(i[1])
                        text = ''.join(i[2])
                        if len(ques) == 0 and len(text) != 3:
                            if text in flammable:
                                index_ques = flammable.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                flammable_category_Q_and_A[index_ques] = cat_temp
                            elif text in flammable_old:
                                index_ques = flammable_old.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                flammable_category_Q_and_A[index_ques] = cat_temp
                            else:
                                if text == '':
                                    continue
                                elif text in flammable:
                                    index_ques = flammable.index(text)
                                    flammable_category_Q_and_A[index_ques] = cat_temp
                                elif text in flammable_old:
                                    index_ques = flammable_old.index(text)
                                    flammable_category_Q_and_A[index_ques] = cat_temp

                        elif ques != "@":
                            #print(ques)
                            if ques in flammable:
                                index_ques = flammable.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                flammable_category_Q_and_A[index_ques] = cat_temp
                            elif ques in flammable_old:
                                index_ques = flammable_old.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                flammable_category_Q_and_A[index_ques] = cat_temp
                            else:
                                if ques == '':
                                    continue
                                elif ques in flammable:
                                    index_ques = flammable.index(ques)
                                    flammable_category_Q_and_A[index_ques] = cat_temp
                                elif ques in flammable_old:
                                    index_ques = flammable_old.index(ques)
                                    flammable_category_Q_and_A[index_ques] = cat_temp

                    #Money
                    if i[0] == 'money':
                        ques = ''.join(i[1])
                        text = ''.join(i[2])
                        if len(ques) == 0 and len(text) != 3:
                            if text in money:
                                index_ques = money.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                money_category_Q_and_A[index_ques] = cat_temp
                            elif text in money_old:
                                index_ques = money_old.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                money_category_Q_and_A[index_ques] = cat_temp
                            else:
                                if text == '':
                                    continue
                                elif text in money:
                                    index_ques = money.index(text)
                                    money_category_Q_and_A[index_ques] = cat_temp
                                elif text in money_old:
                                    index_ques = money_old.index(text)
                                    money_category_Q_and_A[index_ques] = cat_temp

                        elif ques != "@":
                            if ques in money:
                                index_ques = money.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                money_category_Q_and_A[index_ques] = cat_temp
                            elif ques in money_old:
                                index_ques = money_old.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                money_category_Q_and_A[index_ques] = cat_temp
                            else:
                                if ques == '':
                                    continue
                                elif ques in money:
                                    index_ques = money.index(ques)
                                    money_category_Q_and_A[index_ques] = cat_temp
                                elif ques in money_old:
                                    index_ques = money_old.index(ques)
                                    money_category_Q_and_A[index_ques] = cat_temp

                    #perils
                    if i[0] == 'peril' or i[0] == 'perils':
                        ques = ''.join(i[1])
                        text = ''.join(i[2])
                        #print(ques)
                        #print("---")
                        #print(text)
                        if len(ques) == 0 and len(text) != 3:
                            if text in perils:
                                index_ques = perils.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                perils_category_Q_and_A[index_ques] = cat_temp
                            elif text in perils_old:
                                index_ques = perils_old.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                perils_category_Q_and_A[index_ques] = cat_temp
                            else:
                                if text == '':
                                    continue
                                elif text in perils:
                                    index_ques = perils.index(text)
                                    perils_category_Q_and_A[index_ques] = cat_temp
                                elif text in perils_old:
                                    index_ques = perils_old.index(text)
                                    perils_category_Q_and_A[index_ques] = cat_temp

                        elif ques != "@":
                            #print(ques)
                            if ques in perils:
                                index_ques = perils.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                perils_category_Q_and_A[index_ques] = cat_temp
                            elif ques in perils_old:
                                index_ques = perils_old.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                perils_category_Q_and_A[index_ques] = cat_temp
                            else:
                                if ques == '':
                                    continue
                                elif ques in perils:
                                    index_ques = perils.index(ques)
                                    perils_category_Q_and_A[index_ques] = cat_temp
                                elif ques in perils_old:
                                    index_ques = perils_old.index(ques)
                                    perils_category_Q_and_A[index_ques] = cat_temp

                    #security
                    if i[0] == 'security':
                        ques = ''.join(i[1])
                        text = ''.join(i[2])
                        if len(ques) == 0 and len(text) != 3:
                            if text in security:
                                index_ques = security.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                security_category_Q_and_A[index_ques] = cat_temp
                            elif text in security_old:
                                index_ques = security_old.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                security_category_Q_and_A[index_ques] = cat_temp
                            else:
                                if text == '':
                                    continue
                                elif text in security:
                                    index_ques = security.index(text)
                                    security_category_Q_and_A[index_ques] = cat_temp
                                elif text in security_old:
                                    index_ques = security_old.index(text)
                                    security_category_Q_and_A[index_ques] = cat_temp

                        elif ques != "@":
                            if ques in security:
                                index_ques = security.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                security_category_Q_and_A[index_ques] = cat_temp
                            elif ques in security_old:
                                index_ques = security_old.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                security_category_Q_and_A[index_ques] = cat_temp
                            else:
                                if ques == '':
                                    continue
                                elif ques in security:
                                    index_ques = security.index(ques)
                                    security_category_Q_and_A[index_ques] = cat_temp
                                elif ques in security_old:
                                    index_ques = security_old.index(ques)
                                    security_category_Q_and_A[index_ques] = cat_temp

                    #UTILITY
                    if i[0] == 'utility':
                        ques = ''.join(i[1])
                        text = ''.join(i[2])
                        if len(ques) == 0 and len(text) != 3:
                            if text in utilities:
                                index_ques = utilities.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                utilities_category_Q_and_A[index_ques] = cat_temp
                            elif text in utilities_old:
                                index_ques = utilities_old.index(text)
                                cat_temp = []
                                cat_temp.append(text)
                                cat_temp.append(i[3])
                                utilities_category_Q_and_A[index_ques] = cat_temp
                            else:
                                if text == '':
                                    continue
                                elif text in utilities:
                                    index_ques = utilities.index(text)
                                    utilities_category_Q_and_A[index_ques] = cat_temp
                                elif text in utilities_old:
                                    index_ques = utilities_old.index(text)
                                    utilities_category_Q_and_A[index_ques] = cat_temp

                        elif ques != "@":
                            if ques in utilities:
                                index_ques = utilities.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                utilities_category_Q_and_A[index_ques] = cat_temp
                            elif ques in utilities_old:
                                index_ques = utilities_old.index(ques)
                                cat_temp = []
                                cat_temp.append(ques)
                                cat_temp.append(i[3])
                                utilities_category_Q_and_A[index_ques] = cat_temp
                            else:
                                if ques == '':
                                    continue
                                elif ques in utilities:
                                    index_ques = utilities.index(ques)
                                    utilities_category_Q_and_A[index_ques] = cat_temp
                                elif ques in utilities_old:
                                    index_ques = utilities_old.index(ques)
                                    utilities_category_Q_and_A[index_ques] = cat_temp



        #print(sum(x is not None for x in perils_category_Q_and_A) != 4)
        #If some modules are empty
        if sum(x is not None for x in property_category_Q_and_A) != 15:
            property_category_Q = prop
            #print(sum(x is not None for x in property_category_Q_and_A) == 15)
        if sum(x is not None for x in facility_category_Q_and_A) != 13:
            facility_category_Q = facility
        if sum(x is not None for x in fire_category_Q_and_A) != 13:
            fire_category_Q = fire
        if sum(x is not None for x in flammable_category_Q_and_A) != 7:
            flammable_category_Q = flammable
        if sum(x is not None for x in money_category_Q_and_A) != 9:
            money_category_Q = money
        if sum(x is not None for x in perils_category_Q_and_A) != 4:
            perils_category_Q = perils

        if sum(x is not None for x in security_category_Q_and_A) != 20:
            security_category_Q = security
        if sum(x is not None for x in utilities_category_Q_and_A) != 5:
            utilities_category_Q = utilities

        #edge case for facility new question
        if facility_category_Q_and_A[2] == None:
            facility_category_Q_and_A[2] = ['Please mention the commodity stored', ['']]

        #Flammble same question
        if flammable_category_Q_and_A[6] == None:
            flammable_category_Q_and_A[6] = ['What is the storage type used ?', ['']]

        cat_list = ['Fire', 'Smoke', 'Explosion', 'Lightning', 'Flood','Storm', 'Goods', 'Electric', 'Falling Trees', 'Malicious Damage', 'Theft', 'Inception Hazards', 'Spread Hazards', 'Fire Load', 'Planned Structural/Occupational changes']

        add= ''
        country = ''
        if property_category_Q_and_A[0] != None:
            for i in property_category_Q_and_A[0]:
                add = ''.join(i)

            word_list = add.split() 
            country = word_list[-1]




        return render_template('user_data_edit.html', 
        property_category_Q_and_A = property_category_Q_and_A,property_category_Q = property_category_Q,
        facility_category_Q_and_A = facility_category_Q_and_A, facility_category_Q = facility_category_Q,
        fire_category_Q_and_A = fire_category_Q_and_A, fire_category_Q = fire_category_Q,
        flammable_category_Q_and_A = flammable_category_Q_and_A, flammable_category_Q = flammable_category_Q,
        money_category_Q_and_A = money_category_Q_and_A, money_category_Q = money_category_Q,
        perils_category_Q_and_A = perils_category_Q_and_A, perils_category_Q = perils_category_Q,
        security_category_Q_and_A = security_category_Q_and_A, security_category_Q = security_category_Q,
        utilities_category_Q_and_A = utilities_category_Q_and_A, utilities_category_Q = utilities_category_Q,
        cat_list = cat_list, final_list_main=final_list_main,
        user_data = user_data, asset_id = asset_id, asset_name=asset_name,
        address = add, country = country, counter = counter)


@app.route('/user_database/<users_data>', methods=["GET", "POST"])
def user_details(users_data):
    
    #getting asset details
    asset_details = []
    doc = db.document(u'users/'+str(users_data)+'/userPolicies/assets')
    doc = doc.get()
    if doc.exists:
        asset_details.append(doc.to_dict())
    else:
        print(u'No such document!')

    #print(asset_details)

    

    return render_template('all_user_details.html',users_data=users_data, asset_details = asset_details)

@app.route('/user_assets_database', methods=["GET", "POST"])
def user_assets_database():
    user_id = []
    user_name = []
    user = []
    users_data = []
    asset_data = []
    asset_names_list = []
    asset_names = []
    

    users_ref = db.collection(u'users')
    docs = users_ref.stream()
    #print(docs)
    for doc in docs:
        #print(f'{doc.id} => {doc.to_dict()}')
        user_id = doc.id
        user_name = doc.to_dict()
        user_name = list(user_name.values())
        #print(user_name)
        user = [user_id], user_name

        #users_data.append(user)
        users_data.append(user_id)
        
    for uid in users_data:
        doc = db.document(u'users/'+str(uid)+'/userPolicies/assets')
        doc = doc.get()
        if doc.exists:
            #print(f'Document data: {doc.to_dict()}')
            #print(f'{doc.id} => {doc.to_dict()}')
            asset_data.append([doc.to_dict()])
        else:
            print(u'No such document!')

    for i in asset_data:
        for asset_list in i:
            asset_names_list.append(asset_list)

    for asset_list in asset_names_list:
        #print(asset_list )
        asset_names_temp = []
        for key in asset_list:
            asset_names_temp.append(key)
        asset_names.append(asset_names_temp)
        

    #print(asset_names)


    return render_template('user_assets_database.html', asset_data = asset_data, users_data = users_data, asset_names = asset_names)

@app.route('/user_database', methods=["GET", "POST"])
def user_database():
    user_id = []
    user_name = []
    user = []
    users_data = []

    users_ref = db.collection(u'users')
    docs = users_ref.stream()
    #print(docs)
    for doc in docs:
        #print(f'{doc.id} => {doc.to_dict()}')
        user_id = doc.id
        user_name = doc.to_dict()
        user_name = list(user_name.values())
        #print(user_name)
        user = user_id, *user_name
        users_data.append(user)
       
    #print(users_data)
    return render_template('user_database.html', user_id = user_id, user_name = user_name, users_data=users_data)

if __name__ == "__main__":
    app.secret_key='12345'
    app.run(debug = True) 